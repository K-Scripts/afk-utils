package de.fearnixx.afkutils.api.state;

public interface IClientEnterAFK extends IAFKEvent {

    EnterAFKReason getReason();

    enum EnterAFKReason {
        IDLE_EXCEEDED,
        STATUS_SET,
        CHANNEL_ENTERED,
        CONNECT_RESUME,
        OTHER
    }
}
