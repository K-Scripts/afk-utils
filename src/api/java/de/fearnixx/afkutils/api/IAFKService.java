package de.fearnixx.afkutils.api;

import de.fearnixx.jeak.teamspeak.data.IClient;

import java.util.List;

/**
 * Service interface for {@link de.fearnixx.jeak.service.IServiceManager#provide(Class)}
 */
public interface IAFKService {

    /**
     * Returns all clients who are currently tracked as being AFK.
     * @implNote return value is immutable! Use {@link de.fearnixx.afkmove.api.state.IAFKEvent} when needed.
     */
    List<IClient> getClientsCurrentlyAFK();

    /**
     * @see #isAFK(Integer)
     */
    boolean isAFK(IClient client);

    /**
     * Whether or not a client is currently tracked as being AFK.
     */
    boolean isAFK(Integer clientID);

    /**
     * Whether or not a specific channel id is registered as an AFK channel.
     */
    boolean isAFKChannel(Integer channelId);

    /**
     * What channel would be used as the target AFK channel if that client went away now.
     */
    int getSuitableAFKChannel(IClient client);

    boolean isIgnoredChannel(int channelId);

    boolean isIgnoredChannel(int channelId, boolean checkParents);

    void ignoreChannel(int channelId);

    void unignoreChannel(int channelId);
}
