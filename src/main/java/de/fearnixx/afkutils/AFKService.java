package de.fearnixx.afkutils;

import de.fearnixx.afkutils.action.AFKActionListener;
import de.fearnixx.afkutils.api.IAFKService;
import de.fearnixx.afkutils.state.AFKState;
import de.fearnixx.afkutils.watcher.CancelAFKWatcher;
import de.fearnixx.afkutils.watcher.ImmediateAFKWatcher;
import de.fearnixx.afkutils.watcher.PauseAFKWatcher;
import de.fearnixx.afkutils.watcher.UpdateAFKWatcher;
import de.fearnixx.jeak.reflect.IInjectionService;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IChannel;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class AFKService implements IAFKService {

    private static final Logger logger = LoggerFactory.getLogger(AFKService.class);
    private final CancelAFKWatcher cancelAFKWatcher;
    private final PauseAFKWatcher pauseAFKWatcher;
    private final UpdateAFKWatcher updateAFKWatcher;
    private final ImmediateAFKWatcher immediateAFKWatcher;

    @Inject
    public IInjectionService injectionService;

    @Inject
    public IEventService eventService;

    @Inject
    public IDataCache cache;

    private final Object STATE_LOCK = new Object();

    private final AFKConfiguration config;
    private final AFKActionListener actionListener;

    private final Map<Integer, AFKState> clientStates = new ConcurrentHashMap<>();

    public AFKService(AFKConfiguration config) {
        this.config = config;
        this.actionListener = new AFKActionListener(config);
        cancelAFKWatcher = new CancelAFKWatcher(this);
        pauseAFKWatcher = new PauseAFKWatcher(this);
        updateAFKWatcher = new UpdateAFKWatcher(this);
        immediateAFKWatcher = new ImmediateAFKWatcher(this);
    }

    public void initialize() {
        injectionService.injectInto(actionListener);
        eventService.registerListener(actionListener);

        injectionService.injectInto(cancelAFKWatcher);
        injectionService.injectInto(pauseAFKWatcher);
        injectionService.injectInto(updateAFKWatcher);
        injectionService.injectInto(immediateAFKWatcher);
        eventService.registerListeners(cancelAFKWatcher, pauseAFKWatcher, updateAFKWatcher, immediateAFKWatcher);
    }

    @Override
    public List<IClient> getClientsCurrentlyAFK() {
        return Collections.unmodifiableList(
                cache.getClients()
                        .stream()
                        .filter(client -> {
                            AFKState state = clientStates.getOrDefault(client.getClientID(), null);
                            return state != null && state.isAfk();
                        })
                        .collect(Collectors.toList())
        );
    }

    @Override
    public boolean isAFK(IClient client) {
        return isAFK(client.getClientID());
    }

    @Override
    public synchronized boolean isAFK(Integer clientID) {
        synchronized (STATE_LOCK) {
            AFKState state = clientStates.getOrDefault(clientID, null);
            return state != null && state.isAfk();
        }
    }

    public synchronized Optional<AFKState> getState(Integer clientId) {
        synchronized (STATE_LOCK) {
            return Optional.ofNullable(clientStates.getOrDefault(clientId, null));
        }
    }

    private AFKState getOrCreateState(IClient client) {
        return getOrCreateState(client.getClientID(), client.getClientUniqueID());
    }

    private AFKState getOrCreateState(Integer clientID, String uniqueId) {
        return clientStates.computeIfAbsent(clientID, id -> new AFKState(uniqueId));
    }

    public void untrackState(AFKState state) {
        synchronized (STATE_LOCK) {
            // Remove only THAT SPECIFIC state.
            clientStates.entrySet().removeIf(entry -> entry.getValue() == state);
        }
    }

    public void trackState(Integer forClientId, AFKState state) {
        synchronized (STATE_LOCK) {
            clientStates.put(forClientId, state);
        }
    }

    public boolean isChannelExempted(Integer channelID) {
        return (!config.getOnlyCheckChannels().isEmpty() && !config.getOnlyCheckChannels().contains(channelID))
                || config.getAfkChannels().containsKey(channelID.toString())
                || config.getIgnoredChannels().contains(channelID);
    }

    public boolean isAFKChannel(Integer channelID) {
        return config.getAfkChannels().containsKey(channelID.toString());
    }

    public int getSuitableAFKChannel(IClient client) {

        // Skip moving if already in ignored channel
        Integer currentChannelID = client.getChannelID();
        if (isChannelExempted(currentChannelID)) return -1;

        Map<String, Integer> prios = new HashMap<>();

        client.getGroupIDs().forEach(id -> config.getAfkChannels().forEach((channelID, channelNode) -> {
            List<IConfigNode> groups = channelNode.getNode("groups").asList();
            if (groups.isEmpty() || groups.stream().anyMatch(node -> node.asInteger().equals(id))) {
                prios.put(channelID, channelNode.getNode("priority").optInteger().orElse(1));
            }
        }));

        String[] targetChannel = new String[]{null};
        int[] currentPriority = new int[]{0};
        prios.forEach((channelID, priority) -> {
            if (priority > currentPriority[0]) {
                targetChannel[0] = channelID;
                currentPriority[0] = priority;
            }
        });

        if (targetChannel[0] != null) {
            return Integer.parseInt(targetChannel[0]);
        }

        logger.warn("No suiting AFK channel for client: {}", client.getNickName());
        return -1;
    }

    @Override
    public boolean isIgnoredChannel(int channelId) {
        return isIgnoredChannel(channelId, true);
    }

    @Override
    public boolean isIgnoredChannel(int channelId, boolean checkParent) {
        synchronized (config) {
            return isChannelExempted(channelId) || checkIgnoredChannel(channelId, checkParent, config.getIgnoredChannels());
        }
    }

    private boolean checkIgnoredChannel(int channelId, boolean checkParent, List<Integer> ignoredChannels) {
        if (!cache.getChannels().isEmpty()) {
            if (!ignoredChannels.contains(channelId)) {
                IChannel channel = cache.getChannelMap().getOrDefault(channelId, null);

                if (channel != null && checkParent && channel.getParent() > 0) {
                    return checkIgnoredChannel(channel.getParent(), true, ignoredChannels);

                } else if (checkParent && (channel == null || channel.getParent() != 0)) {
                    logger.debug("Channel not ignored as we cannot get his parents: {}", channelId);
                }

                return false;
            } else {
                return true;
            }
        } else {
            // Channel cache is empty: Ignore ALL channels for this run!
            logger.debug("Force-ignored channel: {} - cache not ready.", channelId);
            return true;
        }
    }

    @Override
    public void ignoreChannel(int channelId) {
        synchronized (config) {
            config.getIgnoredChannels().add(channelId);
        }
    }

    @Override
    public void unignoreChannel(int channelId) {
        synchronized (config) {
            config.getIgnoredChannels().remove((Integer) channelId);
        }
    }

    public Object getSTATE_LOCK() {
        return STATE_LOCK;
    }

    public AFKConfiguration getConfiguration() {
        return config;
    }
}
