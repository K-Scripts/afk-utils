package de.fearnixx.afkutils.state;

import de.fearnixx.afkutils.api.state.IClientEnterAFK.EnterAFKReason;
import de.fearnixx.afkutils.api.state.IClientLeaveAFK.LeaveAFKReason;

import java.time.LocalDateTime;

public class AFKState {

    private final String ts3UniqueId;
    private LocalDateTime expiry;

    private boolean isAfk;
    private EnterAFKReason enterReason;
    private LeaveAFKReason leaveReason;

    public AFKState(String ts3UniqueId) {
        this.ts3UniqueId = ts3UniqueId;
    }

    public String getTs3UniqueId() {
        return ts3UniqueId;
    }

    public LocalDateTime getExpiry() {
        return expiry;
    }

    public void setExpiry(LocalDateTime expiry) {
        this.expiry = expiry;
    }

    public boolean isAfk() {
        return isAfk;
    }

    public void setAfk(boolean afk) {
        isAfk = afk;
    }

    public EnterAFKReason getEnterReason() {
        return enterReason;
    }

    public void setEnterReason(EnterAFKReason enterReason) {
        this.enterReason = enterReason;
    }

    public LeaveAFKReason getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(LeaveAFKReason leaveReason) {
        this.leaveReason = leaveReason;
    }
}
