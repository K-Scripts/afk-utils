package de.fearnixx.afkutils.state;

import de.fearnixx.afkutils.api.state.IClientEnterAFK;
import de.fearnixx.jeak.teamspeak.data.IClient;

public class ClientEnterAFK extends AFKEvent implements IClientEnterAFK {

    private final EnterAFKReason enterAFKReason;

    public ClientEnterAFK(IClient client, EnterAFKReason enterAFKReason) {
        super(client);
        this.enterAFKReason = enterAFKReason;
    }

    @Override
    public EnterAFKReason getReason() {
        return enterAFKReason;
    }
}
