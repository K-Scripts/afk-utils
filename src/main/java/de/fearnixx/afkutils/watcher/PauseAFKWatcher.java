package de.fearnixx.afkutils.watcher;

import de.fearnixx.afkutils.AFKService;
import de.fearnixx.afkutils.api.state.IClientLeaveAFK;
import de.fearnixx.afkutils.state.AFKState;
import de.fearnixx.afkutils.state.ClientLeaveAFK;
import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.event.IEventService;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * When a client looses the connection, AFK must be paused and resumed later on.
 */
public class PauseAFKWatcher {

    @Inject
    private IEventService eventService;

    private final Object STATE_LOCK;

    private AFKService afkService;
    private final List<AFKState> pausedStates = new LinkedList<>();

    public PauseAFKWatcher(AFKService afkService) {
        this.afkService = afkService;
        STATE_LOCK = afkService.getSTATE_LOCK();
    }

    @Listener
    public void onClientLeave(IQueryEvent.INotification.IClientLeave event) {
        Integer reasonId = event.getProperty("reasonid")
                .map(Integer::parseInt)
                .orElse(0);

        if (reasonId == 3) {
            // Connection lost -> Only this reasonId is handled by this watcher
            afkService.getState(event.getTarget().getClientID()).ifPresent(state -> {
                synchronized (STATE_LOCK) {
                    state.setAfk(false);
                    state.setEnterReason(null);
                    state.setLeaveReason(IClientLeaveAFK.LeaveAFKReason.DISCONNECT_PAUSE);
                    state.setExpiry(LocalDateTime.now().plusHours(1));

                    pausedStates.add(state);
                    afkService.untrackState(state);

                    ClientLeaveAFK leaveAFK = new ClientLeaveAFK(event.getTarget(), IClientLeaveAFK.LeaveAFKReason.DISCONNECT_PAUSE);
                    eventService.fireEvent(leaveAFK);
                }
            });
        }
    }
}
