package de.fearnixx.afkutils.command;

import de.fearnixx.afkutils.AFKAutomationPlugin;
import de.fearnixx.afkutils.AFKService;
import de.fearnixx.afkutils.state.AFKState;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.ICommandExecutionContext;
import de.fearnixx.jeak.service.command.spec.ICommandExecutor;
import de.fearnixx.jeak.service.command.spec.ICommandSpec;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.util.List;
import java.util.stream.Collectors;

import static de.fearnixx.jeak.service.command.spec.Commands.commandSpec;
import static de.fearnixx.jeak.service.command.spec.Commands.paramSpec;

public class AFKInfoCommand implements ICommandExecutor {

    private final Object STATE_LOCK;
    private final AFKService afkService;

    @Inject
    private IDataCache dataCache;

    public AFKInfoCommand(AFKService afkService) {
        STATE_LOCK = afkService.getSTATE_LOCK();
        this.afkService = afkService;
    }

    public ICommandSpec getCommandSpec() {
        return commandSpec("afk-info", "afkutils:afk-info")
                .parameters(paramSpec().optional(paramSpec("client", IClient.class)))
                .executor(this)
                .permission("afkutils.commands.afk-info")
                .build();
    }

    @Override
    public void execute(ICommandExecutionContext ctx) throws CommandException {
        var optClient = ctx.getOne("client", IClient.class);

        List<String> afkInfos = null;
        if (optClient.isPresent()) {
            afkInfos = List.of(serializeAfkInfoForClient(optClient.get()));
        } else {
            afkInfos = dataCache.getClients()
                    .stream()
                    .map(this::serializeAfkInfoForClient)
                    .collect(Collectors.toList());
        }

        ctx.getConnection().sendRequest(
                ctx.getSender().sendMessage("AFK-State-Info: \n" + String.join("\n", afkInfos)));
    }

    private String serializeAfkInfoForClient(IClient client) {
        synchronized (STATE_LOCK) {
            var optState = afkService.getState(client.getClientID());
            if (optState.isEmpty()) {
                return client.toString() + ": <untracked>";
            } else {
                AFKState state = optState.get();
                return String.format("%s [isAfk: %b, enter-reason: %s, leave-reason: %s, moved-from: %s, idle: %d]",
                        client,
                        state.isAfk(),
                        state.getEnterReason().name(), state.getLeaveReason().name(),
                        client.getProperty(AFKAutomationPlugin.IDENT_MOVED_FROM),
                        client.getIdleTime());
            }
        }
    }
}
