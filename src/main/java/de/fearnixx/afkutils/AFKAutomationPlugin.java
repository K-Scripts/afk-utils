package de.fearnixx.afkutils;

import de.fearnixx.afkutils.api.IAFKService;
import de.fearnixx.afkutils.command.AFKIgnoreCommand;
import de.fearnixx.afkutils.command.AFKInfoCommand;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.IServiceManager;
import de.fearnixx.jeak.service.command.ICommandService;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JeakBotPlugin(id = "afkutils")
public class AFKAutomationPlugin extends Configurable {

    public static final String IDENT_MOVED_FROM = "afkutils::from";
    public static final String IDENT_REASON = "afkutils::reason";

    private static final Logger logger = LoggerFactory.getLogger(AFKAutomationPlugin.class);
    private static final String DEFAULT_CONF_URI = "/afkmove/assets/conf/afkutils.json";

    @Inject
    private IServiceManager serviceManager;

    @Inject
    private IEventService eventService;

    @Inject
    private IInjectionService injectionService;

    @Inject
    private ICommandService commandService;

    @Inject
    @Config
    private IConfig configRef;

    private AFKConfiguration configuration;
    private AFKService service;

    public AFKAutomationPlugin() {
        super(AFKAutomationPlugin.class);
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
        }

        if (getConfig().isVirtual()) {
            logger.info("Configuration is empty, cancelling initialization.");
            event.cancel();
            saveConfig();
        }

        if (!event.isCanceled()) {
            configuration = AFKConfiguration.fromConfig(getConfig());
            injectionService.injectInto(configuration);
            service = new AFKService(configuration);
            serviceManager.registerService(IAFKService.class, service);

            AFKIgnoreCommand ignoreCommand = new AFKIgnoreCommand();
            injectionService.injectInto(ignoreCommand);
            commandService.registerCommand("afk-ignore", ignoreCommand);
            commandService.registerCommand(ignoreCommand.getCommandSpec());
            AFKInfoCommand afkInfoCommand = new AFKInfoCommand(service);
            injectionService.injectInto(afkInfoCommand);
            commandService.registerCommand(afkInfoCommand.getCommandSpec());

            injectionService.injectInto(service);
            service.initialize();
            eventService.registerListener(service);
        }
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected String getDefaultResource() {
        return DEFAULT_CONF_URI;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        return false;
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }
}
